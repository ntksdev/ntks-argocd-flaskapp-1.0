import unittest
import requests

class AppTest(unittest.TestCase):
  def setUp(self):
    self.url = 'http://10.12.202.48/flaskapp'

  def test_welcome(self):
    print('\ntest_welcome')
    response = requests.get(self.url)
    status_code = response.status_code
    content = response.content.decode('ascii')

    self.assertEqual(status_code, 200)
    self.assertIn('Hello, viewers', content)
    self.assertNotIn('Welcome, viewers', content)
    ip_regex = 'IP address of the server is ([0-9]{1,3}\.){3}[0-9]{1,3} !!'
    self.assertRegex(content, ip_regex)

if __name__ == '__main__':
  unittest.main()
