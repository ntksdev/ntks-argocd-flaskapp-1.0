import unittest
import requests
import socket

class AppTest(unittest.TestCase):
  def setUp(self):
    ip = socket.gethostbyname(socket.gethostname())
    self.url = 'http://' + ip + ':5000'

  def test_welcome(self):
    print('\ntest_welcome')
    response = requests.get(self.url)
    status_code = response.status_code
    content = response.content.decode('ascii')

    self.assertEqual(status_code, 200)
    self.assertIn('Hello, viewers', content)

  def test_welcome_negative(self):
    print('\ntest_welcome_negative')
    response = requests.get(self.url)
    status_code = response.status_code
    content = response.content.decode('ascii')

    self.assertEqual(status_code, 200)
    self.assertNotIn('Welcome, viewers', content)

  def test_ip(self):
    print('\ntest_ip')
    response = requests.get(self.url)
    status_code = response.status_code
    content = response.content.decode('ascii')

    self.assertEqual(status_code, 200)
    ip_regex = 'IP address of the server is ([0-9]{1,3}\.){3}[0-9]{1,3} !!'
    self.assertRegex(content, ip_regex)

if __name__ == '__main__':
  unittest.main()
