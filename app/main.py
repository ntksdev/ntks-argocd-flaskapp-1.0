from flask import Flask
import socket

ip = socket.gethostbyname(socket.gethostname())

app = Flask(__name__)

@app.route('/')
def home():
  out = "<!DOCTYPE html><head><title>NTKS CI/CD Demo with Argo</title></head>"
  out += "<body><h1>Hello, viewers!</h1>"
  out += "<p>This page was deployed using Argo CD and helm charts!</p>"
  out += "<p>IP address of the server is " + ip + " !!</p>"
  out += "</body></html>"
  return out

if __name__ == '__main__':
  app.run(debug=True, host='0.0.0.0', port='5000')
